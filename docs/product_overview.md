# Product Overview

## Product Perspective

The product is divided in several modules, which enable a standardized
IAM system integration to operate and exchange identity and identity
data within the Gaia-X environment and trust framework. 

The modules form altogether the IAM SSI Adoption Shell, which
encapsulates the functionality needed for SSI-based interactions and
protocols, translating them into flows and data formats understandable
by the Standard IAM. The main components of this shell are the SSI OIDC
Broker and the SSI IAT Provider which support the SSI based user login
and the SSI based dynamic client registration.

To visualize the product's cooperation, the functionality was drawn in
the figure below ([**Figure 1**](#fig_architecture)) as an ArchiMate
cooperation view together with the business process of user login and
client registration.

![diagram](media/image2.png)

**Figure 1**: Sketch of the Functional Overview of the SSI Adoption
Shell

As seen in [**Figure 2**](#fig_Overview), the target of the product is
to adopt an existing IAM by adding SSI based modules to the standard
OpenID Connect and OAuth2 compliant IAM. The expected behavior for end
users and applications is described as the following:

![diagram](media/image3.png)

**Figure** **2**: Sketch of the Behavior of the SSI Adoption Shell

[**Figure 2**](#fig_Overview) shows that the additional modules have a
hard connection to the SSI based application backend which must work as
a kind of SSI sidecar for the standard IAM. All actions by the user or
by any Back end is triggered in this direction.
<!-- The “Trust Services” and
the “Organizational Credential Manager” are separate lots of the
IDM&Trust Package and not scope of this product. --> 
The interaction of the
product in this direction must be exclusively over standard internet
HTTPS protocol calls.

## Product Functions

Authentication and Authorization components are anchored into the big
picture of the IAM architecture for Architecture ducument and form together a core Gaia-X IAM
building block.

[![diagram](media/image4.png)](https://app.diagrams.net/?page-id=5f0bae14-7c28-e335-631c-24af17079c00&scale=auto#G1GOBR7fL4I47PdcXUbS0MK4E74Oo-jOyL)

**Figure** **3**: Product Functions

The core functions are:

-   SSI based Issuing of Initial Access Tokens (IAT)
-   SSI based Login

Both functions are realized with a backchannel authentication which is
triggered over the trust services API. This API provides a stateless
policy evaluation API which connects the relevant back-channel
authentication points over HTTP in the background. The API delivers all
information which has to be proofed on the client side over the
backchannel. This is inspired by the principle of OpenID Connect Client
Initiated Backchannel Authentication
OIDC.CIBA[^1]. The user agents have in this case
to poll to evaluate the login state of their action by a kind of ticket
ID. Pull and Ping flows are out of scope. In both cases the users have
to trigger the proof over the backchannel, or the proof is timed out.

-   Other functions like DID SIOP [^2] can follow in the future if the
    specification is done by the OpenID foundation. 
    [^1]:N. Sakimura, J. Bradley, M. Jones, B. de Medeiros, C. Mortimore. (2014), OpenID Connect Core 1.0 incorporating errata set 1. <(https://openid.net/specs/openid-client-initiated-backchannel-authentication-core-1_0.html )>
    [^2]:K. Yasuda, M. Jones, T. Lodderstedt. (2022), Self-Issued OpenID Provider v2. <(https://identity.foundation/did-siop/)>

## Product Constraints

⏩ IDM.AA.00001 **The document IDM.AO is the common basis for this
functional specification**

> The architecture document IDM.AO is an
> essential part of this specification and a prerequisite for
> understanding the context. The specifications and requirements from
> the Architecture Document MUST be considered during implementation. ⏪

## User Classes and Characteristics

|                |                                                                                                     |             |             |                   |                               |
|----------------|-----------------------------------------------------------------------------------------------------|-------------|-------------|-------------------|-------------------------------|
| *User Class*   | *Description*                                                                                       | *Frequency* | *Expertise* | *Privilege Level* | *Product Usage*               |
| Administrator  | The administrator installs the product and configures it in the existing IAM system.                | Low         | High        | High              | Maintenance                   |
| External Users | External users are using the product for login.                                                     | High        | Low         | Low               | SSI Based Login               |
| External APIs  | External APIs can use the product to authenticate via SSI to obtain an IAT for client registration. | Low         | High        | Low               | SSI Based Client Registration |

**Table** **2**: User Classes and Characterstics

## Operating Environment

<!-- Please refer to \[TDR\] for further binding requirements regarding the
operating environment. -->

⏩ IDM.AA.00002 **TLS Protected Endpoints**

> To protect the product endpoint(s), it’s necessary to support a
> network infrastructure e.g., load balancers/proxies which MUST support
> TLS encryption. The encryption MUST meet the requirements listed in
> the chapter for security requirements. ⏪

⏩ IDM.AA.00003 **NTP Server on Stratum Level**

> The product MUST be operated within an environment connected to a
> trusted stratum level 2/3 NTP Server. ⏪

<!--
## User Documentation

Please refer to \[TDR\] for further requirements regarding
documentation.

⏩ IDM.AA.00004 **Participant Administration Documentation**

> The documentation MUST contain:
> 
> -   Installation Manuals
> 
> -   Cryptographic Initialization (if applicable)
> 
> -   Description of Deployment/Compile Process
> 
> -   Description of the Automatic Tests / Verification
> 
> -   How to build the products from source code 
⏪

⏩ IDM.AA.00005 **Participant Documentation**

The documentation MUST contain:

-   Short Software Description (why and for what, when to use, how use,
    where to use)

-   Usage guide

-   GDPR design decisions

-   Security concept

-   Operations concept

-   FAQ

-   Keyword Directory ⏪
-->

## Assumptions and Dependencies

The main assumptions behind this SSI adoption shell are that the shell
is running in the same security domain as the standard OIDC compatible
IAM system. All components MUST be built on top of the OAuth2
RFC6749[^3] and OIDC standard
OIDC.Core.

An understanding of the overall Gaia-X architecture and philosophy is
necessary. Please refer to the Architecture ducument.
[^3]: Internet Engineering Task Force (IETF) (2012), The OAuth 2.0 Authorization Framework. <(https://www.rfc-editor.org/rfc/rfc6749)>
## Apportioning of Requirements

All Features have to be delivered in the first version.
