# Requirements



## External Interfaces

⏩ IDM.AA.00006 **Trust Services API**

> The Trust Services API is an API which evaluates policies in the
> background. In this scenario the API is used to check the proof state
> of the given presentationID to find out if the proof is valid or not.
> The API is returning a REST Codes and JSON structure with the
> associated proof information. ⏪

⏩ IDM.AA.00007 **OIDC Interface**

> The OIDC Interface provides a standard OIDC flow which follows the
> open id connect protocol 1.0. ⏪

⏩ IDM.AA.00008 **Token Interface**

> The token interface is a token endpoint of a standard IAM system and
> provides functionalities to obtain token with different token flows.
> This token endpoint MUST follow the OAuth2 authorization framework
> spec RFC6749. ⏪

### User Interfaces

The SSI OIDC Broker interface provides a web page for login with a QR
Code. (see more in the functional chapter below)

### Software Interfaces

The product described within this specification does not expose any
specific software interfaces other than communication interfaces covered
in [Communications Interfaces](#communications-interfaces).

The choice of operating system, database system, tools and libraries are
left open to the provider, as long as other requirements within this
specification, especially those described in [Product
Constraints](product_overview.md#product-constraints) and [Operating Environment](product_overview.md#operating-environment) are
fulfilled.

### Communications Interfaces

#### SSI OIDC Broker

##### Offered Interfaces

⏩ IDM.AA.00009 **OIDC Provider**

SSI OIDC Broker offers OpenID Connect provider interface for integration
of authentication and authorization flows with any compatible
application or identity brokering IAM system.

All offered HTTP interfaces are following
OIDC.Core and OIDC.Discovery [^1] as appropriate and specified
in the feature description part of this specification. ⏪

[^1]:N. Sakimura, J. Bradley, M. Jones, E. Jay. (2014), OpenID Connect Discovery 1.0 incorporating errata set 1.<(https://openid.net/specs/openid-connect-discovery-1_0.html)>

##### Consumed Interfaces

⏩ IDM.AA.00010 **Trust Services API**

SSI OIDC Broker consumes policy evaluation interfaces of Trust Services
API IDM.TSA [^2] for back channel DIDComm-based
authentication and authorization. ⏪

[^2]: Gaia-X Trust Framework. (2022). <https://gaia-x.gitlab.io/policy-rules-committee/trust-framework/>

#### SSI IAT Provider

##### Offered Interfaces

<span id="i5k7o67lrd4c" class="anchor"></span>⏩ IDM.AA.00011 **SSI Client Registration Auth API**

SSI IAT Provider offers a HTTP-based REST API which allows for obtaining
RFC7591 [^3] compliant Initial Access Token (IAT)
for client registration at an OAuth2 Authorization Server.
⏪

[^3]: Internet Engineering Task Force (IETF) (2015). OAuth 2.0 Dynamic Client Registration Protocol.<(OAuth 2.0 Dynamic Client Registration Protocol)>

##### Consumed Interfaces

⏩ IDM.AA.00013 **OAuth2 token interface**

SSI OIDC Broker connects to a standard IAM OAuth2 endpoint, using client
credentials grant with appropriate scope, to generate new
RFC7591 Initial Access Tokens. ⏪

## Functional

### Common

<span id="5kk3fbdrbn0d" class="anchor"></span>⏩ IDM.AA.00014
**Credential Based Access Control (CrBac)**

> The SSI adoption shell SHOULD be able to dynamically reload
> credentials for access decisions, for instance the current identity
> wants a “sales” action and is currently just logged in with a
> “visitor” permission. The CrBac SHOULD be able to resolve this by
> requesting new credentials. This might be achieved either by a renewed
> authentication and authorization flow triggered by the application
> (via SSI OIDC Provider), or via an asynchronous process, which SHOULD
> be done over standard IAM outgoing PIP interface towards Trust
> Services or the It MAY be realized over additional components within
> the architecture, but the Standard IAM MUST NOT be modified (excepting
> configuration, plugins or supported extensions).
>
> Acceptance Criteria

1.  During a resource access, it must be demonstrated that new
    credentials can be transmitted to get access

2.  The documentation must describe what to configure in the
    demonstration IAM ⏪

⏩ IDM.AA.00015 **External PIP Integration**

> It MUST be demonstrated how an external PIP with asynchronous behavior
> can be integrated in the authorization services of a standard IAM
> system. It MAY be demonstrated with additional components. ⏪

### SSI OIDC Broker

⏩ IDM.AA.00016 <span id="njox283x6qs4" class="anchor"></span>**Standard open source IAM Package**

> Together with the Adoption Shell, the product MUST include a working
> integration with one basic open source IAM system. The selection has
> to be compliant with the Gaia-X policy and rules and any choice that
> supports the OAuth2 RFC7591 standard
> functions as Client Registration RFC7591 ,
> Token Issuing (minimum code and client credential flow), authorization
> and permission handling.
>
> Acceptance Criteria

1.  One working IAM System with the Adoption Shell

2.  Documentation of proper configuration of both Adoption Shell and
    chosen IAM system

3.  “All in one” package, ready for installation

⏪

<span id="vpjlfxj8j5mn" class="anchor"></span>⏩ IDM.AA.00017 **OpenID Provider Configuration Information**

> SSI OIDC Provider MUST offer an OpenID Provider Configuration
> Information endpoint as specified in
> OIDC.Discovery. All SSI-related scopes and
> claim types SHOULD be exposed through this endpoint for dynamic
> discovery and configuration. The SSI OIDC Provider MUST fulfill the
> requirements of OpenID Provider Publishing Configuration Information
> profile as in OIDC.Conformance [^4].
>
> Acceptance Criteria

1.  Documentation of self-conducted conformance testing of OpenID
    Provider Publishing Configuration Information profile or an official
    certification from OpenID Foundation

2.  Documentation how the Well-Known Configuration has to be setup

3.  Demonstrate a working dynamic configuration of a standard IAM OIDC
    broker with OpenID Provider Configuration Information of SSI OIDC
    Provider

4.  Explanation in the operations concept how to configure it
[^4]: OpenID Connect Working Group, OpenID Foundation (2018), OpenID Connect Conformance Profiles v3.0. <(https://openid.net/wordpress-content/uploads/2018/06/OpenID-Connect-Conformance-Profiles.pdf)>

⏪

<span id="lkn9j1lwgys" class="anchor"></span>⏩ IDM.AA.00018 **OpenID Connect Implicit profile**

> SSI OIDC Provider MUST fulfill all the requirements of Implicit OpenID
> Provider profile as defined in OIDC.Conformance.
>
> Acceptance Criteria

1.  Documentation of self-conducted conformance testing of Implicit
    OpenID Provider profile or an official certification from OpenID
    Foundation

2.  Demonstrate a working integration with a standard IAM brokering OIDC
    Authorization to SSI OIDC Provider with OpenID Connect Implicit
    profile configuration.

3.  The documentation must describe what to configure in the
    demonstration IAM

⏪

<span id="fynehgnmpqed" class="anchor"></span>⏩ IDM.AA.00019 **OAuth 2.0 Security Best Current Practice**

> SSI OIDC Provider SHOULD employ all relevant measures for security of
> OAuth2.0 framework BCP OAuth2 [^5].
>
> Acceptance Criteria:

4.  Documentation of applied measures as per BCP
    OAuth2
[^5]: T. Lodderstedt, J. Bradley, A. Labunets, D. Fett (2020),  OAuth 2.0 Security Best Current Practice draft-ietf-oauth-security-topics-16. <(https://tools.ietf.org/html/draft-ietf-oauth-security-topics-16)>
⏪

⏩ IDM.AA.00020 <span id="ojv09cuzys85" class="anchor"></span>**SSI Login Page**

> The OIDC Provider MUST contain a customizable Standard Login web page
> integrated into OIDC Authorization flow and endpoint as per
> OIDC.Core which shows an QR Code for login
> using another device hosting Personal Credential Manager as well as a
> button with the same link being able to open Personal Credential
> Manager on the same device. The button MUST trigger a registered
> application or page on the new browser tab, without interruption of
> the count-down and the status polling process.
>
> The SSI Login Page MUST display a progress bar which counts down a
> configurable time (e.g., 30 seconds).
>
> The login page MUST poll in the background the configured login state
> URL with a given request identifier for the configured amount of time.
>
> If the request is successful or timed-out during the waiting time the
> SSI Login Page MUST redirect the User Agent back to the IAM system
> redirect\_url with appropriate response parameters as per
> OIDC.Core.
>
> This login page MUST be styleable over CSS or html template system to
> customize the look and feel at the installation and configuration
> time. The login page should support internationalization and follow
> either browser settings or OIDC parameters defined for this purpose.
> OIDC parameters take precedence. Minimum set of supported languages:
> English, German, French.
>
> Acceptance Criteria

1.  The login page presents an QR Code and the Button Link with an SSI
    invitation link/proof request

2.  The login page redirects with an error response after the time-out

3.  The login page polls in the background the login state and redirects
    to the given URL after a successful login

4.  The login page must deliver with one specific Look & Feel aligned
    with Gaia-X Portal UX. Information regarding Look & Feel of the
    Gaia-X Portal UX will be provided by eco.

5.  The login page must support required languages and display according
    to the browser settings or OIDC authorization request parameters as
    per precedence definition

6.  The documentation must describe Look & Feel customization options
    and their configuration

⏪

⏩ IDM.AA.00021 <span id="litweym7qo93" class="anchor"></span>**QR Code Generation**

> The QR code contains the content of the SSI invitations/proofs, which
> MUST be obtained from an external URL with the values for scope and a
> value for “Namespace”. Scope Values MUST be extracted from the
> authorize request. The URL request body SHOULD follow the following
> pattern matching the requirements of IDM.TSA [Appendix B](appendix.md) GetLoginProofInvitation Policy:
>
> 
```
{
    “scope”:
    [
        “openid”,
        “gaia-x.user”,
        ”gaia-x.role”,
        ”gaia-x.organization.membership”,...
    ]
    “Namespace”: “AdministrationLogin”,                     // Client
    “sub”: “did:example:123123123”,                         // Optional reference
    “not\_older\_than”: “2021-04-01T01:23:45.678+00:00”,    // Optional ttl of the required proof
    “max\_age”: 3600                                        // Maximum Authentication Age. Specifies the 
                                                            // allowable elapsed time in seconds since 
                                                            // the last time the End-User was actively
                                                            // authenticated
}
```
>
> The response body follows the following pattern:
>
```
{
    “presentationID”:”....”,
    “link”:”...”
}
```
>
> The link content has to be generated in a QR Code. PresentationID
> needs be securely stored in the browser session, so that it’s
> available for the [IDM.AA.00022 Login State Background
> Polling](#IDM.AA.00022-Login-State-Background-PollingLogin-State-Background-Polling) process and not revealed outside of this
> context as it represents a secure token to get identity data.
>
> Acceptance Criteria

1.  The button and the QR Code contain the link content rendered as QR
    Code

2.  The QR Code must be readable by a smartphone

3.  The presentation ID is <u>not</u> inside the QR Code or button link

⏪

⏩ IDM.AA.00022 <span id="57agswltgke5" class="anchor"></span>**Login State Background Polling**

> During the shown Login page, a background task MUST poll for the state
> of the current presentationID created in [IDM.AA.00021 QR Code Generation](#qR-code-generation). This HTTP request MUST execute in the
> background a request for the state with the following pattern in the
> request body to Trust Services API IDM.TSA [Appendix](appendix.md) GetLoginProofResult Policy:
>
```
{
    “presentationID”:”....”,
}
```
> The response body SHOULD follows the following pattern:

1.  No State: HTTP 204

2.  Done: HTTP 200

```
{
    “iss”: “...”,
    “sub”: “...”,
    “claim1”: “...”
    ...
    “claimx”: “...”
}
```

3.  Error response: HTTP 40x

> The content in the response MUST be available in the IAM system after
> the successful response. This MUST be realized by continuing the
> standard OIDC flow and forming a valid id\_token including the claims
> from the response.
>
> Unsuccessful response shall be followed with an option to retry the
> process with [IDM.AA.00021 QR Code Generation](#qR-code-generation) or to fail the process and redirect
> back to the IAM with appropriate OIDC failure response.
>
> Acceptance Criteria

1.  Login Token with the contained claims and scopes

2.  Correctly signed token

3.  Redirect to IAM System on Success

4.  Offer Retry or redirect back with failure to IAM System on Error

⏪

<span id="9nzshg3udpzd" class="anchor"></span>⏩ IDM.AA.00023 **Session
handling and scope elevation**

> SSI OIDC Provider MUST employ configurable session handling allowing
> multiple authorization requests for the same identity being seamlessly
> handled without a need to authenticate the user again for a
> pre-defined time period of session validity. id\_token\_hint parameter
> as specified in OIDC.Core SHOULD be
> supported.
>
> In case additional scopes are required the SSI OIDC Provider MUST
> conduct a proof request for the additional Verifiable Credentials
> without a need to build a new connection with an invitation QR
> Code/Link. The option to create a new Link MUST be available, however.
>
> Session handling related parameters of
> OIDC.Core, like prompt or max\_age shall
> be respected and translated into appropriate proof requests to assure
> required functionality.
>
> The proof is realized with the same methods and policies as described
> in [IDM.AA.00021 QR Code Generation](#qR-code-generation) and
> [IDM.AA.00022 Login State Background
> Polling](#IDM.AA.00022-Login-State-Background-PollingLogin-State-Background-Polling) using optional sub and max\_age
> parameters.
>
> Acceptance Criteria:

1.  session handling by consecutive authentication requests with
    id\_token\_hint do not impose new authentication and authorization
    and authenticate the user in a seamless manner

2.  a request for additional proofs is conducted and added to id\_token
    in case the requested scope of authorization is wider than
    previously

3.  Session duration is configurable at installation / deployment time

⏪

### SSI IAT Provider

⏩ IDM.AA.00024 **Offer SSI Client Registration Auth API**

An API as per [IDM.AA.00011 SSI Client Registration Auth
API](#offered-interfaces) MUST be offered to enable initiation and
polling for the result of SSI-based issuance of IAT for Dynamic Client
Registration.

> Acceptance Criteria

1.  SSI Client Registration API is offered and documented

⏪

<span id="9u0liim6r122" class="anchor"></span>⏩ IDM.AA.00025 **Policy
based authorization**

> The SSI IAT Provider MUST integrate with Trust Services to conduct
> policy authorization checks of the client trying to obtain an Initial
> Access Token (IAT). IAT MUST not be issued unless the policy
> evaluation allows for that operation.
>
> The following Policies have to be executed by this authorization flow:

-   IDM.TSA [Appendix B](#appendix.md) GetIatProofInvitation - flow
    initiation

-   IDM.TSA [Appendix B](appendix.md) GetIatProofResult Policy -
    polling for a result

> Acceptance Criteria

2.  IAT is issued only after successful evaluation of the Policy by
    Trust Services

3.  negative evaluation of the Policy by Trust Services results in no
    IAT issued and an appropriate error response

⏪

<span id="99uitfty1mzv" class="anchor"></span>⏩ IDM.AA.00026 **Standard
IAM Compatibility**

> The issued Initial Access Token MUST be compatible with the Client
> Registration Endpoint of the docked standard IAM. It MUST be possible
> to register with this IAT client as defined in RFC7591.
>
> Acceptance Criteria

1.  A dynamic client with an IAT can be registered in the IAM system
    over the client registration endpoint.

⏪

<span id="klb58aoyw9a3" class="anchor"></span>⏩ IDM.AA.00027 **Client
Registration**

> The IAT Provider MUST be registered as an OAuth2
> RFC6750 [^6] Client using client credential
> grant within the Standard IAM to obtain Initial Access Tokens of the
> System.
>
> Acceptance Criteria
[^6]:Internet Engineering Task Force (IETF) (2012), The OAuth 2.0 Authorization Framework: Bearer Token Usage. <()https://tools.ietf.org/html/rfc6750>
1.  A dynamic client with a software statement can be registered in the
    IAM system over the client registration endpoint.

⏪

## Other Nonfunctional Requirements

⏩ IDM.AA.00028 **Security Hardening**

> The whole adoption shell is security relevant, and it has to be
> defined in the security concept how these components can be more
> secured and what kind of steps to do.

⏪

### HTTP Requirements

⏩ IDM.AA.00029 **HTTPS**

> All HTTP Endpoints MUST be protected by TLS 1.3 (2018) (all protocol version
> numbers SHOULD be superseded by upcoming standards) Each endpoint of
> the product MUST support TLS certificates which are configurable by
> the administrator of the system. ⏪

⏩ IDM.AA.00030 **HTTP Protocol Definitions**

> All HTTP Endpoints MUST follow RFC7231 [^7]and
> RFC5789 [^8], but it MAY be chosen what of the
> protocols is necessary to realize the functionality. For problem
> reports the RFC7807[^9] MUST be used in
> combination with Standard HTTP Error Codes.⏪
[^7]: Internet Engineering Task Force (IETF) (2014), Hypertext Transfer Protocol (HTTP/1.1): Semantics and Content. <(https://tools.ietf.org/html/rfc7231)>
[^8]: Internet Engineering Task Force (IETF) (2010), PATCH Method for HTTP. <(https://tools.ietf.org/html/rfc5789)>
[^9]:Internet Engineering Task Force (IETF) (2016), Problem Details for HTTP APIs. <(https://tools.ietf.org/html/rfc7807)>
### Configuration

⏩ IDM.AA.00031 **Configuration**

> Refer to the 12Factors approach [^10]. All components MUST support one of the major configuration formats
> (yaml, json, ini, environment variables) wherever configuration is
> required. If environment variables are overwriting an actively set
> configuration, a warning SHOULD be logged. ⏪
[^10]: The Twelve-Factor App (2010). <(https://tools.ietf.org/html/rfc5789)>
### Logging Requirements

⏩ IDM.AA.00032 **Data Minimization**

> From GDPR perspective the product MUST NOT log data which is related
> to personal information. (e.g., User Names, Birth Dates etc. ) The
> product MUST only log data, which is relevant to technical operations,
> except for the purpose that, in the event of an incident, enable
> reconstruction of the sequence of the message exchange for
> establishing the place and the nature of the incident. The data shall
> be stored for a period of time in accordance with national
> requirements and, as a minimum, shall consist of the following
> elements:
>
> \(a\) node's identification
>
> \(b\) message identification
>
> \(c\) message data and time
>
> All logged data/information MUST be documented in the GDPR design
> decisions for a GDPR review.

⏪

⏩ IDM.AA.00033 **Logging Frameworks**

> The product MUST use standard RFC5224 [^11]log format and MUST
use standard outputs. 
[^11]: R. Gerhards (2009). The Syslog Protocol. <(https://www.rfc-editor.org/rfc/rfc5424)>

⏪

### Monitoring Requirements

⏩ IDM.AA.00034 **Monitoring Frameworks**

> The product MUST be compliant with OpenTelemetry tooling [^12]. ⏪
[^12] : <(https://opentelemetry.io/)>
⏩ IDM.AA.00035 **Alerting Frameworks**

> Additional to the Monitoring Frameworks an Alerting framework (e.g.,
> Prometheus or Cloud Based) MUST/MAY be in place at least in the System
> nodes to promptly communicate to e.g., System Administrators or owners
> the occurrence of an event in form of a security incident or
> application/system malfunction or anomaly. ⏪

### Performance Requirements

⏩ IDM.AA.00036 **Performance Scalability**

> The performance of the product MUST be scalable. This MUST be
> demonstrated in a load demonstration example. The optimal scalability
> SHOULD be in the best case a linear behavior of minimum 50% more
> performance by each additional instance. ⏪

⏩ IDM.AA.00037 **Performance by Design**

> The product SHOULD be designed and implemented in a way, that the
> implementation is non-blocking and performance oriented. It SHOULD be
> a microservice architecture, but it MAY follow other concepts. The
> decision MUST be documented. ⏪

### Safety Requirements

⏩ IDM.AA.00038 **Recovery Point Objective (RPO)**

> The RPO for the product MUST be 0 for a single and multiple
> instance(s). It MAY be higher by configuration or deployment, decided
> by the user. ⏪

⏩ IDM.AA.00039 **Recovery Time Objective (RTO)**

> The RTO for the product MUST be one Minute for a single instance. For
> multiple instances the RTO MUST be 0. ⏪

⏩ IDM.AA.00040 **Mitigation of Single Point of Failure threats**

> Critical components in the Gaia-X Ecosystem MUST be identified and
> strategies to warranty their availability and scalability MUST be
> implemented. ⏪

### Security Requirements

#### General Security Requirements

Each Gaia-X Federation Service SHALL meet the requirements stated in the
document “Specification of non-functional Requirements Security and
Privacy by Design”. Federation Services specific
requirements will be documented in the next chapter.

#### Service Specific Security Requirements

This chapter will describe the service specific requirements, which will
extend the requirements defined in the chapter above.

⏩ IDM.AA.00041 **Cryptographic Algorithms and Cipher Suites**

> Cryptographic algorithms and TLS cipher suites SHALL be chosen based
> on the recommendation from The European Union Agency for Cybersecurity (ENISA). These recommendations and the
> recommendations of other institutions and standardization organization
> are quite similar CryptoLen[^13]. The
> recommendations can be found in the technical guidelines TR 02102-1[^14]
>  and TR 02102-2[^15] or SOG-IS Agreed Cryptographic
> Mechanisms SOG-IS[^16]. ⏪
[^13] : Damien Giry, Prof. Jean-Jacques Quisquater (2020), Cryptographic Key Length Recommendation. <(https://www.keylength.com/)>
[^14]:BSI (2022), Cryptographic Mechanisms: Recommendations and Key Lengths BSI TR-02102-. <(https://www.bsi.bund.de/SharedDocs/Downloads/EN/BSI/Publications/TechGuidelines/TG02102/BSI-TR-02102-1.pdf?__blob=publicationFile&v=2)>
[^15]: BSI (2022), Cryptographic Mechanisms: Recommendations and Key Lengths: Use of Transport Layer Security (TLS) BSI TR-02102-2. <(https://www.bsi.bund.de/SharedDocs/Downloads/EN/BSI/Publications/TechGuidelines/TG02102/BSI-TR-02102-2.pdf?__blob=publicationFile&v=2)>
[^16]SOG-IS Crypto Working Group (2020), SOG-IS Crypto Evaluation Scheme - Agreed Cryptographic Mechanisms. <(https://www.sogis.eu/documents/cc/crypto/SOGIS-Agreed-Cryptographic-Mechanisms-1.2.pdf)>

⏩ IDM.AA.00042 **Digital Certificates**

> For digital certificates and cryptographic signatures in the context,
> the major requirements on cryptographic algorithms and key length MUST
> meet the definitions in the following table (as of 2020):

<table>
<tbody>
<tr class="odd">
<td><blockquote>
<p><strong>Signature Algorithm</strong></p>
</blockquote></td>
<td><blockquote>
<p><strong>Key size</strong></p>
</blockquote></td>
<td><blockquote>
<p><strong>Hash function</strong></p>
</blockquote></td>
</tr>
<tr class="even">
<td>EC-DSA</td>
<td>Min. 250 Bit</td>
<td>SHA-2 with an output length ≥ 256 Bit or better</td>
</tr>
<tr class="odd">
<td><p>RSA-PSS (recommended)</p>
<p>RSA-PKCS#1 v1.5 (legacy)</p></td>
<td>Min. 3000 Bit RSA Modulus (n) with a public exponent e &gt; 2^16</td>
<td>SHA-2 with an output length ≥ 256 Bit or better</td>
</tr>
<tr class="even">
<td>DSA</td>
<td><p>Min. 3000 Bit prime p</p>
<p>250 Bit key q</p></td>
<td>SHA-2 with an output length ≥ 256 Bit or better</td>
</tr>
</tbody>
</table>

**Table** **3**: Requirements on cryptographic algorithms and key length

> Named curves SHALL be used for EC-DSA (e.g., NIST-p-256). ⏪

⏩ IDM.AA.00043 **TLS Certificate Validity Periods**

> In general, the recommended validity period for a certificate used in
> the system should be one year or less. Under some circumstances (for
> example RootCA) the certificate validity can be extended. Certificate
> owners MUST ensure that valid certificates are renewed and replaced
> before their expiration to prevent service outages.⏪

⏩ IDM.AA.00044 **Security by Design**

> The software security MUST be from the beginning a design principle.
> Means separation of concerns, different administrative roles,
> especially for private key material and separate access to the data
> MUST be covered from the first second. It MUST be described in the
> security concept, what are the different security risks of the product
> and how they are mitigated (e.g., by Threat Modeling Protocols) ⏪

⏩ IDM.AA.00045 **Installation of Critical Security Updates**

> Node operators SHALL deploy security critical updates without undue
> delay. ⏪

⏩ IDM.AA.00046 **Avoid** **HTTP Request Smuggling**

> To avoid Request Smuggling attacks, the product MUST implement a
> standard which handles this kind of attack by design, because the
> attack vector results in an insufficient implementation of the header
> handling. The chosen way to handle it MUST be shared to the other
> implementers of all other subcomponents within IDM & Trust and MUST be
> described in the security concept. ⏪

⏩ IDM.AA.00047 **HTTP Pentesting**

> All HTTP parts of the product has to be pen tested, for the following
> criteria:

1.  Unauthorized Access to the System MUST be tested

2.  Unauthorized Actions MUST be triggered without a user action

3.  Endpoints MUST be tested for HTTP smuggling attack vectors

4.  If a datastore is present over HTTP, illegal data access MUST be
    tested

> It’s RECOMMENDED to test more attack vectors and document it for the
> purpose to mitigate it in later versions. ⏪

⏩ IDM.AA.00048 **Storage of Secrets**

> The storage of secret information such as private keys MUST take place
> in state-of-the-art secure environments to protect secret data
> confidentiality and integrity. Examples of this are Secure Enclaves,
> TPMs, HSM or Secure Vaults. In case (Personal) Agents are not equipped
> with a secure storage it MAY also be possible to store the secrets in
> a third party (e.g., Cloud) provider (e.g., Secure Wallet) that MUST
> provide overall the same level of security as the aforementioned
> methods. ⏪

⏩ IDM.AA.00049 **Secret Distribution and Usage**

> The product MUST ensure interoperability of cryptographic primitives
> and components by public standards and MUST use secure state of the
> art methods to create and import secrets into the secure storage, as
> well as performing cryptographic operations (e.g., encryption or
> digital signatures). For Key distribution, state of the art DKMS
> methods MUST be implemented. ⏪

⏩ IDM.AA.00050 **Support for Potential Requirements for Secret
Storages**

> Devices that hold cryptographic information and perform cryptographic
> functions MUST be compliant with the standard PKCS \#11. Moreover, the
> products MUST be potentially eligible for a
> FIPS-140-2[^17] or ETSI/Common Criteria
> certification with the minimum-security level necessary to operate
> securely in the Gaia-X ecosystem. Security Levels in FIPS-140-2 range
> from 1 to 4. Current HSM Cloud Service offerings (AWS, Azure, GCP) are
> Level 3. ⏪
[^17]:NIST (2001) FIPS 140--2, Security Requirements for Cryptographic Modules .<(http://csrc.nist.gov/publications/PubsFIPS.html#140-2)>
⏩ IDM.AA.00051 **Secure Timestamps**

> All timestamps MUST be issued according to
> RFC3161[^18]. ⏪
[^18]:C. Adams, P. Cain, D. Pinkas, R. Zuccherato (2001), Internet X.509 Public Key Infrastructure, Time-Stamp Protocol (TSP). <(https://www.ietf.org/rfc/rfc3161.txt)>
⏩ IDM.AA.00052 **Special Availability and Scalability Requirements for
Secret Storage Components**

> Secret Storage components play a central role in storage, encryption,
> and digital signing in the Gaia-X ecosystem, thus they can become a
> single point of failure for a Gaia-X participant, for example an
> organization. Therefore, methods and procedures to ensure the
> availability and scalability of the Secret Storage functionality MUST
> be implemented. ⏪

### 3.3.8. Software Quality Attributes

⏩ IDM.AA.00053 **Quality Aspects**

> The software MUST meet the following requirements:

-   The quality standards MUST meet ISO 25010 ISO25000[^19]

-   Robustness / Reliability

-   Performance

-   Availability must be 24/7

-   Interoperability with the other work packages

-   Security

-   Adaptability / expandability

-   Maintainability and Code Quality

-   Scalability

> <u>Major</u> security concerns regarding design and implementation
> MUST be documented and highlighted to the steering board*.*
> <u>Minor</u> security concerns SHALL be documented and mitigated. ⏪
[^19]: ISO 25000 Portal (n.d.), ISO/IEC 25010;<(https://iso25000.com/index.php/en/iso-25000-standards/iso-25010)>.
### Business Rules

Business Rules are defined by external components, especially by
IDM.TSA.

## Compliance

⏩ IDM.AA.00054 **GDPR Audit Logging**

All GDPR relevant access to personal relevant data MUST be logged for a
later audit. ⏪

⏩ IDM.AA.00055 **GDPR Data Processing**

> Is it necessary to process person-relevant data, it MUST be earmarked
> to a clearly defined business process, which has to be described in
> the GDPR design decisions. All person relevant data MUST be deleted
> after the processing, if applicable. ⏪

## Design and Implementation

Please also refer to \[TDR\] for further requirements.

### Installation

⏩ IDM.AA.00056 **Micro Service Architecture**

> For a better scale out and decentralization, the product architecture
> MUST a micro service architecture.⏪

⏩ IDM.AA.00057 **Independent Service**

> All Adoption Shell functions MUST be separately deployable web
> applications, which communicate with other systems either over exposed
> HTTP-based APIs or consume HTTP-based APIs. The components MUST NOT be
> built into any particular IAM system in a way of extension or plugin,
> so that it’s always possible to connect them to any IAM system which
> is offering the needed APIs.
>
> Acceptance Criteria

1.  All functions of the Adoption Shell are deployable as standalone web
    applications ⏪

### Distribution

⏩ IDM.AA.00058 **Config Data Distribution**

> The product SHOULD support a global data distribution of config data
> to synchronize configurations between multiple regions in the world.
> Built-in synchronization technology (asynchronous and synchronous) MAY
> be used. ⏪

### Maintainability

⏩ IDM.AA.00059 **Micro Service Architecture**

> For a better scale out, maintainability and decentralization, the
> product architecture MUST have a micro service architecture. Each
> microservice MUST NOT be limited on the lines of code or number of
> days to implement it. The service “size” SHOULD be oriented on the
> fine granular business capabilities. (e.g., Order, ListMenu, Payment).
> ⏪

⏩ IDM.AA.00060 **Domain Driven Design**

> To support the micro service architecture within the maintainability,
> it MUST be declared a domain model before realization. The software
> description MUST explain which domain model was chosen, which services
> contain it and how it scales. This MUST be documented in the public
> code repository to support future enhancements for new developers. ⏪

### Operability

⏩ IDM.AA.00061 **FTE Estimation**

> The product MUST be designed so that over scripts and tools one FTE
> within a Month SHOULD host and operate the product without any
> third-party help. It MUST be sketched in the operations concept how
> this can be achieved. If this target is not reachable it MAY be
> explained and described why the effort is higher and appropriate. ⏪

### Interoperability

⏩ IDM.AA.00062 **Interoperability of IT security features and
algorithms**

> The following interoperability requirements of the respective IT
> security features and algorithms MUST be ensured across the system
> components:

-   Interoperability of crypto algorithms and protocols (including the
    novel peer-reviewed ones through the established bodies and
    communities)

-   Interoperability of secure secret transfer protocols (such as the
    holistic usage of PKCS\#11 for HSM communication, etc.)

-   Format interoperability of crypto material (such as the holistic
    usage of PKCS\#12 for relevant cases) ⏪
