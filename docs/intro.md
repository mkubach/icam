# Introduction

To get general information regarding Gaia-X and the Gaia-X Federation
Services please refer to Architecture ducument[^1].
[^1]:Gaia-X Architecture. <(https://docs.gaia-x.eu/technical-committee/architecture-document/22.04/)>

## Document Purpose 

This document covers the Gaia-X Identity, Credential and Access management specifications.

## Product Scope

The scope of the document is to describe the components for
“Authorization & Authentication” which shall deliver core
functionalities for authorization, access management and authentication
as well as services around it, to Gaia-X Participants with the purpose
to join the trustful environment of the ecosystem. This document does
not describe how to replace an already established IAM System within a
Gaia-X participant environment or how to operate it within the Gaia-X
participant environment. There is also no claim to replace any existing
IAM or to provide a solution for managing users in the enterprise
environment of the Gaia-X participant.

## Definitions, Acronyms and Abbreviations

The IDM and Trust Architecture Overview Document
IDM.AO[^1]MUST be considered and applied as the core
technical concept that includes also the Terminology and Glossary.



<!-- TODO  move external specs reference to Gaia-X documents, ie include IDM.AO -->

## Document Overview

The document describes the product perspective, functions, and
constraints. It furthermore lists the non-functional
requirements and defines the system features in detail. The listed
requirements are binding. Requirements as an expression of normative
specifications are identified by a unique ID in square brackets (e.g.
**\[IDM.ID.Number\]**) and the keywords MUST, MUST NOT, SHOULD, SHOULD
NOT, MAY, corresponding to RFC2119 [^2],
are written in capital letters.
[^2]:Network Working Group. (1997). Key words for use in RFCs to Indicate Requirement Levels. <(https://datatracker.ietf.org/doc/html/rfc2119)>