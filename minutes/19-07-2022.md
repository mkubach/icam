# Gaia-X TC WG Identity & Access Management - Meeting notes for 19/07/2022

## Minutes

### Closed issues and decisions

### Open issues and questions :

* [Issue 15 : IAM Vocabulary](https://gitlab.com/gaia-x/technical-committee/federation-services/iam/-/issues/15) : See comments in the issue.
* [Issue 17 : Group leaders election](https://gitlab.com/gaia-x/technical-committee/federation-services/iam/-/issues/17) : See comments in the issue.

### Discussion and proposals
* [OpenID Connect compatibility] About SSI - OpenID Connect bridge, Berthold sent us a [link](https://gitlab.com/gaia-x/data-infrastructure-federation-services/authenticationauthorization/-/tree/main) to the implementation made by ECO (compatible with Keycloak).

### Demonstration

Following the previous [demontration](12-07-2022.md#demonstration) we get a demonstration of the authentication part through Keycloak.
The demonstration stack can be installed easily on a local machine by follwing the "Getting Started" from https://gitlab.com/gaia-x/data-infrastructure-federation-services/authenticationauthorization.

A demonstration of the fully integrated chain (from authentication to TSA) can be planned when the integration work is completed.

### Actuality

Working group leader election, see [Issue 17 : Group leaders election](https://gitlab.com/gaia-x/technical-committee/federation-services/iam/-/issues/17).