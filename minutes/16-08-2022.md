# Gaia-X TC WG Identity & Access Management - Meeting notes for 16/08/2022

## Minutes

Reminder of the objectives to be met by the end of the month (in this vacation period).

### Closed issues and decisions

* Nothing

### Open issues and questions :

* [Issue 6 : Define or get a list of supported resolver on DID](https://gitlab.com/gaia-x/technical-committee/federation-services/iam/-/issues/6) : Please vote on last comment.
* [Issue 17 : Group leaders election](https://gitlab.com/gaia-x/technical-committee/federation-services/iam/-/issues/17) : See comments in the issue.
* [Issue 18 : Update IDM.AA](https://gitlab.com/gaia-x/technical-committee/federation-services/iam/-/issues/18) : We have to update IDM.AA document by end of August (see issue comment), **please make proposals via merge requests**.
* [Issue 19 : DIDComm use cases](https://gitlab.com/gaia-x/technical-committee/federation-services/iam/-/issues/19) : Please provide relevant use cases to demonstrate how DIDComm complements OpenID Connect. 
* [Issue 15: Define vocabulary](https://gitlab.com/gaia-x/technical-committee/federation-services/iam/-/issues/15)
* New, to be evaluated: https://gitlab.com/gaia-x/technical-committee/federation-services/iam/-/issues/21
* New, to be evaluated: https://gitlab.com/gaia-x/technical-committee/federation-services/iam/-/issues/20

### Discussion and proposals
* We might to discuss futher if did:web is sufficient for current use case or if we need some more as a working example. 

### Special note 
*  Next meeting will be cancelled due to inavailability of Bastien and Gerd
