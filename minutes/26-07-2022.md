# Gaia-X TC WG Identity & Access Management - Meeting notes for 26/07/2022

## Minutes

### Closed issues and decisions

### Open issues and questions :

* [Issue 6 : Define or get a list of supported resolver on DID](https://gitlab.com/gaia-x/technical-committee/federation-services/iam/-/issues/6) : Please vote on last comment.
* [Issue 17 : Group leaders election](https://gitlab.com/gaia-x/technical-committee/federation-services/iam/-/issues/17) : See comments in the issue.
* [Issue 11 : Discussion about Self Sovereign Authorization](https://gitlab.com/gaia-x/technical-committee/federation-services/iam/-/issues/11) : Closed by linking it to [issue 14](https://gitlab.com/gaia-x/technical-committee/federation-services/iam/-/issues/14).
* [Issue 18 : Update IDM.AA](https://gitlab.com/gaia-x/technical-committee/federation-services/iam/-/issues/18) : We have to update IDM.AA document by end of August (see issue comment), **please make proposals via merge requests**.
* [Issue 19 : DIDComm use cases](https://gitlab.com/gaia-x/technical-committee/federation-services/iam/-/issues/19) : Please provide relevant use cases to demonstrate how DIDComm complements OpenID Connect. 

### Discussion and proposals
* Need to update IDM_AA document (see Issue 18), during this discussion we also discussed the value of DIDComm and how it can complement the OpenID Connect compatibility currently being promoted.
* Supported DID resolution methods : see issue 6 update.
* Update on [IDUnion](https://idunion.org) project status.
