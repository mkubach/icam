# Gaia-X TC WG Identity & Access Management - Meeting notes for 17/05/2022

## Minutes

* IP issue : [the Architecture Document](https://docs.google.com/document/d/1tpIgEXC-3h-N6Xk5NhoWtdfQuXT_wkVITKzNVSaKRdI/edit#heading=h.f26y2rhalgq8) could have an IP issue : a [issue](https://gitlab.com/gaia-x/technical-committee/federation-services/iam/-/issues/10) : See issue for status, Steffen will send an email to ECO in order to get a status.
* The group members are invited to participate on **Authorization** (see [discussion issue](https://gitlab.com/gaia-x/technical-committee/federation-services/iam/-/issues/11)) :
  * By privileging a decentralized mode (like for Authentication)
  * That can be seamlessly integrated with the SSI ecosystem (DID / VC)
  * Than is compatible with SSI <-> OpenID Bridge built for Authentication

* We have to make choices between decentralized and semi-decentralized options to manage Authorization, possible scenarios are :

| Authentication                  | Authorization                                                   | Authorization verification mode                                                                              |
| ------------------------------- | --------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------ |
| DID Auth (decentralized)        | Classic authorization token (group membership, ...) in DID comm | Centralized (I.E. the target service must access to a central repository to compute verification result)     |
| DID Auth (decentralized)        | Self-supporting / Signed token in DID comm                      | Decentralized (I.E the target service has all need informations in the token to compute verification result) |
| DID + OIDC (semi-decentralized) | Classic authorization token (group membership, ...) via OIDC    | Centralized (I.E. the target service must access to a central repository to compute verification result)     |
| DID + OIDC (semi-decentralized) | Self-supporting / Signed token via OIDC                         | Decentralized (I.E the target service has all need informations in the token to compute verification result) |

* Parameters to be considered :
  * **Market adoption** : OIDC based solutions will certainly get adopted more quickly as the protocol is already widely deployed.
  * **Standards maturity** : DID Auth, DID comm are very young and not really stabilized standards
  * **Scalability** : a full decentralized system will certainly scale better, in a world of micro-services / services composition across multiples service providers, the capacity to compute authorization result in an autonomous way is key.
  * **Interoperability** : self-supporting authorization token facilitate services composition across networks / service providers

## Technical demo

Kai Meinke propose to organize a short demo (30') of the Gaia-X Lab demonstrator for OIDC4VP/SIOP flows for onboarding, accreditation and access management during the next meeting (17-05-2022)
> Postponed to a future date
